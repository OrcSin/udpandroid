package com.example.orcsin.udpandroid;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    EditText editTextIpAddress, editTextPort, editTextSendMessage;
    TextView textViewShowMassage, textViewShowStatus;
    Button btnSendAndListen;

    ClientSendAndListen clientSendAndListen;

    String SERVER = "192.168.0.200";
    int SERVER_PORT = 5151;
    String SEND_MESSAGE = "OrcSin";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextIpAddress = (EditText) findViewById(R.id.editTextIpAddress);
        editTextPort = (EditText) findViewById(R.id.editTextPort);
        editTextSendMessage = (EditText) findViewById(R.id.editTextSendMessage);

        textViewShowStatus = (TextView) findViewById(R.id.textViewShowStatus);
        textViewShowMassage = (TextView) findViewById(R.id.textViewShowMassage);

        btnSendAndListen = (Button) findViewById(R.id.btnSendAndListen);
        btnSendAndListen.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSendAndListen:
                if (TextUtils.isEmpty(editTextIpAddress.getText().toString())
                        || TextUtils.isEmpty(editTextPort.getText().toString())
                        || TextUtils.isEmpty(editTextSendMessage.getText().toString())) {
                    return;
                }
                SERVER = editTextIpAddress.getText().toString();
                SERVER_PORT = Integer.parseInt(editTextPort.getText().toString());
                SEND_MESSAGE = editTextSendMessage.getText().toString();

                clientSendAndListen = new ClientSendAndListen();
                clientSendAndListen.execute(SEND_MESSAGE);
                break;
        }
    }

    class ClientSendAndListen extends AsyncTask<String, String, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            textViewShowStatus.setText("Start");
        }

        @Override
        protected Void doInBackground(String... messageText) {
            boolean run = true;
            try {
                DatagramSocket client_socket = new DatagramSocket(SERVER_PORT);
                InetAddress IpServerAddress = InetAddress.getByName(SERVER);
                String text = messageText[0];
                byte[] buf = text.getBytes();
                DatagramPacket sendPacket = new DatagramPacket(buf, text.length(), IpServerAddress, SERVER_PORT);
                client_socket.send(sendPacket);
                while (run) {
                    try {
                        byte[] message = new byte[8000];
                        DatagramPacket resivePacket = new DatagramPacket(message, message.length);
                        client_socket.setSoTimeout(10000);
                        client_socket.receive(resivePacket);
                        String showMessage = new String(message, 0, resivePacket.getLength());
                        publishProgress(showMessage);
                    } catch (SocketException e) {
                        run = false;
                        client_socket.close();
                        e.printStackTrace();
                    } catch (IOException e) {
                        run = false;
                        client_socket.close();
                        e.printStackTrace();
                    }
                }
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            textViewShowMassage.setText("Message " + values[0]);
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            textViewShowStatus.setText("End");
        }
    }
}
